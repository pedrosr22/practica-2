
package mates;
public class Matematicas{
	/*
	 * Copyright [2022] [Pedro Sánchez Rosillo]
	 Licensed under the Apache License, Version 2.0 (the "License");
	 you may not use this file except in compliance with the License.
	 You may obtain a copy of the License at
http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing,
2
*/
	/**
	 * @author Pedro Sánchez 
	 * @param el parametro que le paso es de tipo long, al cual he nombrado pasos que significa en el teorema de montecarlo los dardos que se lanzan.
	 * @return el metedo descrito de generarNumeroPiIterativo() lo que hace es devolver un numero de tipo double es decir con varios decimales, que se debe aproximar lo maximo al numero pi.
	 */
	

	public static double generarNumeroIterativo(long pasos, long i, long dardosDentro){
		if(i<= pasos){

			double x = Math.random();
			double y = Math.random();
			if(Math.pow(x,2) + Math.pow(y,2) <= 1){
				dardosDentro++;
			}
			return generarNumeroIterativo(pasos, i+1, dardosDentro);

		}else{
			return 4*dardosDentro/(double)pasos;
		}

	}
}
